class ImageModel {
  final String id;
  final String created_at;
  final String updated_at;
  final String description;
  final String alt_description;
  final String url;

  ImageModel({
    required this.id,
    required this.created_at,
    required this.updated_at,
    required this.description,
    required this.alt_description,
    required this.url,
  });

  static ImageModel fromJson(Map map) {
    return ImageModel(
      id: map['id'],
      created_at: map['created_at'],
      updated_at: map['updated_at'],
      description: map['description'].toString(),
      alt_description: map['alt_description'],
      url: map['urls']['small'],
    );
  }
}
