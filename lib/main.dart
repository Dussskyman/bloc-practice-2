import 'package:bloc_practice_2/bloc_image/image_bloc.dart';
import 'package:bloc_practice_2/bloc_image/image_bloc_page.dart';
import 'package:bloc_practice_2/models/image.dart';
import 'package:bloc_practice_2/pages/image_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ImageBlocPage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    context.read<ImageBloc>().add(ImageEvent.getImage);
    return BlocBuilder<ImageBloc, List<ImageModel>>(
      builder: (ctx, data) {
        if (data.isNotEmpty) {
          return Scaffold(
            body: GridView.builder(
              itemCount: 10,
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            ImagePage(imageModel: data[index]),
                      ),
                    );
                  },
                  child: Hero(
                    tag: data[index].id.toString(),
                    child: SizedBox(
                      width: 300,
                      height: 300,
                      child: Image.network(data[index].url),
                    ),
                  ),
                );
              },
            ),
          );
        }
        return Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }
}
