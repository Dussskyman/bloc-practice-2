import 'dart:convert';

import 'package:bloc_practice_2/models/image.dart';
import 'package:http/http.dart' as http;

const String _path =
    'https://api.unsplash.com/photos/?client_id=ab3411e4ac868c2646c0ed488dfd919ef612b04c264f3374c97fff98ed253dc9';

class ImageRepo {
  ImageRepo._private();
  static final _instance = ImageRepo._private();
  static ImageRepo get instance => _instance;

  Future<List<ImageModel>> getImages() async {
    print('Started');
    List<ImageModel> imageModels = [];
    await http.get(Uri.parse(_path)).then((value) {
      print(value.body);
      jsonDecode(value.body).forEach((element) {
        imageModels.add(ImageModel.fromJson(element));
      });
    });
    print('finished');
    return imageModels;
  }
}
