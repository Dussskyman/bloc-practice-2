import 'package:bloc_practice_2/models/image.dart';
import 'package:flutter/material.dart';

class ImagePage extends StatelessWidget {
  final ImageModel imageModel;
  const ImagePage({Key? key, required this.imageModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Hero(
            tag: imageModel.id.toString(),
            child: SizedBox(
              height: 300,
              child: Image.network(imageModel.url),
            ),
          ),
          Text(imageModel.description),
          Text(imageModel.alt_description),
          Text(imageModel.created_at),
          Text(imageModel.updated_at)
        ],
      ),
    );
  }
}
