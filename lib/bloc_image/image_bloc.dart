import 'package:bloc_practice_2/models/image.dart';
import 'package:bloc_practice_2/repository/image_repo.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

enum ImageEvent { getImage }

class ImageBloc extends Bloc<ImageEvent, List<ImageModel>> {
  ImageBloc() : super([]);

  @override
  Stream<List<ImageModel>> mapEventToState(ImageEvent event) async* {
    print(event);
    switch (event) {
      case ImageEvent.getImage:
        yield await ImageRepo.instance.getImages();
        break;
      default:
        yield state;
    }
  }
}
