import 'package:bloc_practice_2/bloc_image/image_bloc.dart';
import 'package:bloc_practice_2/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ImageBlocPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ImageBloc(),
      child: MyHomePage(),
    );
  }
}
